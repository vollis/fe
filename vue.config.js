const Timestamp = new Date().getTime();
const outputName = require('./src/util/outputName');
const FileManagerPlugin = require('filemanager-webpack-plugin')

module.exports = {
    lintOnSave: true,
    productionSourceMap: false,
    publicPath: './',
    outputDir: outputName,
    //打包后生成压缩包
    configureWebpack: {
        plugins: [
            new FileManagerPlugin({
                events: {
                    onEnd: {
                        delete: [`./${outputName}.zip`],
                        archive: [{ source: `./${outputName}`, destination: `./${outputName}.zip` }]
                    }
                }
            })
        ],
        output: { // 输出重构  打包编译后的 文件名称  【模块名称.时间戳】
            filename: `js/[name].${Timestamp}.js`,
            chunkFilename: `js/[name].${Timestamp}.js`
        }
    },
    chainWebpack: (config) => {
        //忽略的打包文件
        config.externals({
            'vue': 'Vue',
            'vue-router': 'VueRouter',
            'vuex': 'Vuex',
            'axios': 'axios',
            'element-ui': 'ELEMENT',
        })
        const entry = config.entry('app')
        entry
            .add('babel-polyfill')
            .end()
        entry
            .add('classlist-polyfill')
            .end()
        entry
            .add('@/mock')
            .end()
    },
    devServer: {
        // 端口配置
        port: 8089,
        disableHostCheck: true,
        proxy: {
            '/api': {
                target: 'http://192.168.7.170:8186', //测试服务器
                ws: true,
            },
            '/statics': {
                target: 'http://192.168.7.170:8186',
                ws: true
            },
            '/portrait': {
                target: 'http://192.168.7.170:8186',
                ws: true
            },
        }
    }
}
