

let Timestamp = new Date();

let outputName = 'dist'
if (process.env.NODE_ENV !== 'test') {
    outputName += dateFormat2(Timestamp, "MMdd")
}

function dateFormat2(date, format, rounding) {
    if (date != 'Invalid Date') {
        var o = {
            "M+": date.getMonth() + 1, //month
            "d+": date.getDate(), //day
            "h+": rounding ? 0 : date.getHours(), //hour
            "m+": rounding ? 0 : date.getMinutes(), //minute
            "s+": rounding ? 0 : date.getSeconds(), //second
            "q+": Math.floor((date.getMonth() + 3) / 3), //quarter
            "S": date.getMilliseconds() //millisecond
        }
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
            (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(format))
                format = format.replace(RegExp.$1,
                    RegExp.$1.length == 1 ? o[k] :
                        ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    }
    return '';

}

module.exports = outputName
