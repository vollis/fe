let ThousandSeparator = value => {
    return (
        value &&
        value
            .toString()
            .replace(/(^|\s)\d+/g, (m) => m.replace(/(?=(?!\b)(\d{3})+$)/g, ","))
    );
}

export { ThousandSeparator }