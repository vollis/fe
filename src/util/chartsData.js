import {
    dateFormat2
} from "./date";

/**
 * 图表日期时间段内空数据补0
 */
export function dataSupplement_date(date, interval) {
    let startTime = interval.startTime
    let endTime = interval.endTime
    let times = []
    if (startTime && endTime) {
        while (startTime < endTime) {
            let starDate = new Date(startTime)
            starDate.setDate(starDate.getDate() + 1)
            times.push({
                num: 0, day: startTime
            })
            startTime = dateFormat2(starDate, 'yyyy-MM-dd');
        }
    }
    for (let i = 0; i < times.length; i++) {
        for (let j = 0; j < date.length; j++) {
            if (date[j].day === times[i].day) {
               times[i].num = date[j].num
            }
        }
    }
    return times
}
/**
 * 图表小时时间段内空数据补0
 */
export function dataSupplement_hour(date, interval) {
    let startTime = interval.startTime
    let endTime = interval.endTime
    let times = []
    for (let i = 0; i < 24; i++) {
        let hour = ''
        if (i < 10) {
            hour = '0' + i
        } else {
            hour = '' + i
        }
        times.push({
            hourNum: hour,
            num: 0
        })
    }
    for (let i = 0; i < times.length; i++) {
        for (let j = 0; j < date.length; j++) {
            if (date[j].hourNum === times[i].hourNum) {
               times[i].num = date[j].num
            }
        }
    }
    return times
}
