import Vue from 'vue';
import axios from './router/axios';
import VueAxios from 'vue-axios';
import App from './App';
import router from './router/router';
import './permission'; // 权限
import './error'; // 日志
import store from './store';
import { loadStyle } from './util/util'
import * as urls from '@/config/env';
import Element from 'element-ui';
import $ from "jquery";
Vue.prototype.$ = $

import {
    iconfontUrl,
    iconfontVersion
} from '@/config/env';
import i18n from './lang' // Internationalization
// 样式加载
import 'element-ui/lib/theme-chalk/index.css';

import './styles/common.scss';
import './styles/main.css';
import basicContainer from './components/basic-container/main'
import '@/styles/font.scss';

import _ from 'lodash'
Vue.prototype._ = _

import { baseUrl } from '@/config/env';

import * as custom from './util/filtersNum'

Object.keys(custom).forEach(key => {
    Vue.filter(key, custom[key])
})
import SuperFlow from 'vue-super-flow'
import 'vue-super-flow/lib/index.css'

Vue.use(SuperFlow)
require("./mock")
Vue.prototype.$baseUrl = baseUrl

Vue.use(router)
Vue.use(VueAxios, axios)

Element.TableColumn.props.showOverflowTooltip.default = true

import Avue from '@smallwei/avue';

Vue.use(Avue);
Vue.use(Element, {
    i18n: (key, value) => i18n.t(key, value)
})

//注册全局容器
Vue.component('basicContainer', basicContainer)
// 加载相关url地址
Object.keys(urls).forEach(key => {
    Vue.prototype[key] = urls[key];
})


// 动态加载阿里云字体库
iconfontVersion.forEach(ele => {
    loadStyle(iconfontUrl.replace('$key', ele));
})

Vue.config.productionTip = false;

// 添加滚动方法
Vue.directive('loadmore', {
    bind(el, binding) {
        const selectWrap = el.querySelector('.el-table__body-wrapper')
        selectWrap.addEventListener('scroll', function () {
            let sign = 10
            const scrollDistance = this.scrollHeight - this.scrollTop - this.clientHeight
            if (scrollDistance <= sign) {
                binding.value()
            }
        })
    }
})

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')

