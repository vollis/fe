import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const getList = (current, size, params) => {
  return request({
    url: baseUrl + '/menu/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
export const remove = (ids) => {
  return request({
    url: baseUrl + '/menu/delete',
    method: 'post',
    data: [ids]
  })
}

export const add = (row) => {
  return request({
    url: baseUrl + '/menu/save',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: baseUrl + '/menu/update',
    method: 'post',
    data: row
  })
}

export const getMenu = (id) => {
  return request({
    url: baseUrl + '/menu/info/' + id,
    method: 'get',
    // params: {
    //   id,
    // }
  })
}

export const getMenuTree = () => {
  return request({
    url: baseUrl + '/menu/tree',
    method: 'get'
  })
}
