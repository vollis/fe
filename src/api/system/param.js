import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const getList = (pageNum, pageSize, params) => {
  return request({
    url: baseUrl + '/sysconfig/list',
    method: 'get',
    params: {
      ...params,
      pageNum,
      pageSize,
    }
  })
}
export const remove = (ids) => {
  return request({
    url: baseUrl + '/sysconfig/delete',
    method: 'post',
    data: [ids]
  })
}

export const add = (row) => {
  return request({
    url: baseUrl + '/sysconfig/save',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: baseUrl + '/sysconfig/update',
    method: 'post',
    data: row
  })
}
