import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const getList = (pageNum, pageSize, params) => {
  return request({
    url: baseUrl + '/role/list',
    method: 'get',
    params: {
      ...params,
      pageNum,
      pageSize,
    }
  })
}
export const grantTree = () => {
  return request({
    url: baseUrl + '/menu/list',
    method: 'get',
  })
}

export const grant = (roleIds, menuIds) => {
  return request({
    url: baseUrl + '/role/grant',
    method: 'post',
    data: {
      id:roleIds,
      menuIdList:menuIds
    }
  })
}

export const remove = (ids) => {
  return request({
    url: baseUrl + '/role/delete',
    method: 'post',
    data: [ids]
    })
}

export const add = (row) => {
  return request({
    url: baseUrl + '/role/save',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: baseUrl + '/role/update',
    method: 'post',
    data: row
  })
}


export const getRole = (roleIds) => {
  return request({
    url: baseUrl + '/role/menuTree/'+roleIds,
    method: 'get',
  })
}

export const getRoleTree = (tenantId) => {
  return request({
    url: baseUrl + '/role/tree',
    method: 'get',
    params: {
      tenantId,
    }
  })
}
