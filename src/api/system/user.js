import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const getList = (pageNum, pageSize, params) => {
  return request({
    url: baseUrl + '/user/list',
    method: 'get',
    params: {
      ...params,
      pageNum,
      pageSize,
    }
  })
}
export const remove = (ids) => {
  return request({
    url: baseUrl + '/user/delete',
    method: 'post',
    data: ids
  })
}

export const add = (row) => {
  return request({
    url: baseUrl + '/user/save',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: baseUrl + '/user/update',
    method: 'post',
    data: row
  })
}

export const grant = (userIds, roleIds) => {
  return request({
    url: baseUrl + '/user/grant',
    method: 'post',
    data: {
      idList:userIds,
      roleIdList:roleIds,
    }
  })
}

export const getUser = (id) => {
  return request({
    url: baseUrl + '/user/info/' + id,
    method: 'get',
    // params: {
    //   id,
    // }
  })
}

export const getUserInfo = () => {
  return request({
    url: baseUrl + '/user/getUserInfo',
    method: 'get',
  })
}

export const resetPassword = (userIds) => {
  return request({
    url: baseUrl + '/user/resetPassword',
    method: 'post',
    data: {
      userIds,
    }
  })
}

export const updatePassword = (row) => {
  return request({
    url: baseUrl + '/user/modifyPassword',
    method: 'post',
    data: row
  })
}

export const getIsAdmin = () => {
  return request({
    url: baseUrl + '/sysdict/dictionary?code=is_admin',
    method: 'get'
  })
}

export const updateSelf = (row) => {
  return request({
    url: baseUrl + '/user/updateSelf',
    method: 'post',
    data: row
  })
}
