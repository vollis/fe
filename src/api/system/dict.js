import request from '@/router/axios';
import {baseUrl} from '@/config/env';

export const getList = (current, size, params) => {
  return request({
    url: baseUrl + '/sysdict/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
export const remove = (ids) => {
  return request({
    url: baseUrl + '/sysdict/delete',
    method: 'post',
    data: [ids]
  })
}

export const add = (row) => {
  return request({
    url: baseUrl + '/sysdict/save',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: baseUrl + '/sysdict/update',
    method: 'post',
    data: row
  })
}


export const getDict = (id) => {
  return request({
    url: baseUrl + '/sysdict/info/'+id,
    method: 'get',
    // params: {
    //   id,
    // }
  })
}
export const getDictTree = () => {
  return request({
    url: baseUrl + '/sysdict/tree',
    method: 'get'
  })
}

