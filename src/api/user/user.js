import request from '@/router/axios';
import {baseUrl} from '@/config/env';
import website from "@/config/website";

export const loginByUsername = (row) => request({
  url: baseUrl + '/login',
  method: 'post',
  // headers: {
  //   'Captcha-Key': key,
  //   'Captcha-Code': code,
  // },
  data: row

});

export const getButtons = () => request({
  url: baseUrl + '/menu/buttons',
  method: 'get'
});

export const getUserInfo = () => request({
  url: '/user/getUserInfo',
  method: 'get'
});

export const refeshToken = () => request({
  url: '/user/refesh',
  method: 'post'
})

export const getMenu = () => request({
  url: baseUrl + '/menu/myMenus',
  method: 'get'
});

// export const getCaptcha = () => request({
//   url: baseUrl + '/captcha',
//   method: 'get'
// });
export const getCaptcha = ()=> {return baseUrl + '/captcha?'+Date.now()};
export const getTopMenu = () => request({
  url: '/user/getTopMenu',
  method: 'get'
});

export const sendLogs = (list) => request({
  url: '/user/logout',
  method: 'post',
  data: list
})

export const logout = () => request({
  url: '/api/logout',
  method: 'post'
})
export const getKey = () => request({
  url: '/api/getKey',
  method: 'get'
})

