export default {
    column: [{
        label: '个人信息',
        prop: 'info',
        option: {
            dialogClickModal: false,
            submitText: '修改',
            column: [{
                label: '头像',
                type: 'upload',
                prop: 'photo',
                listType: 'picture-img',
                propsHttp: {
                    home: '/portrait',
                    name: 'resultData',
                    url: 'resultData',
                },
                // canvasOption: {
                //     text: 'blade',
                //     ratio: 0.1
                // },
                action: '/api/wx/uploadPortrait',
                tip: '只能上传jpg/png用户头像，且不超过500kb',
                span: 12,
                row: true,
            }, {
                label: '姓名',
                span: 12,
                row: true,
                prop: 'realname',
            }, {
                label: '用户名',
                span: 12,
                row: true,
                prop: 'username'
            }, {
                label: '手机号',
                span: 12,
                row: true,
                prop: 'mobile',
            }, {
                label: '邮箱',
                prop: 'email',
                span: 12,
                row: true,
            }]
        }
    }, {
        label: '修改密码',
        prop: 'password',
        option: {
            dialogClickModal: false,
            submitText: '修改',
            // disabled: false,
            column: [{
                label: '原密码',
                span: 12,
                row: true,
                type: 'password',
                prop: 'password',
                // disabled: false,

            }, {
                label: '新密码',
                span: 12,
                row: true,
                type: 'password',
                prop: 'newPassword',
                // disabled: false,

            }, {
                label: '确认密码',
                span: 12,
                row: true,
                type: 'password',
                prop: 'newPassword1',
                // disabled: false,

            }]
        }
    }],

}
